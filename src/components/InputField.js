import React from 'react'
import '../css/InputField.css'

const InputField = (props) => (
    <input
        className="inputField"
        type="number"
        step={0.01}
        value={props.value}
        onChange={props.handleChange}
        onFocus={props.handleFocus}
        onBlur={props.handleBlur}
    />
)
export default InputField