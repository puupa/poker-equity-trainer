import React from 'react'
import Card from './Card'
import '../css/Board.css'

const Board = (props) => (
    <div className="Board-container">
        <Card card={props.card1} />
        <Card card={props.card2} />
        <Card card={props.card3} />
        <Card card={props.card4} />
    </div>
)

export default Board