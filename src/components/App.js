import React, { Component } from "react";
import "../css/App.css";
import Header from "./Header";
import Board from "./Board";
import Hand from "./Hand";
import InputField from "./InputField";
import Message from './Message'
import ScoreBoard from './ScoreBoard'
import hands, { equity } from "../data/equity";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      sessionStarted: false,
      handStarted: false,
      showScoreBoard: true,
      showMessage: false,
      timeToAnswer: 0,
      hands: Number(localStorage.getItem('hands')),
      totalDiff: Number(localStorage.getItem('totalDiff')),
      totalTime: Number(localStorage.getItem('totalTime')),
      board1: "./img/cards/card_XX.png",
      board2: "./img/cards/card_XX.png",
      board3: "./img/cards/card_XX.png",
      board4: "./img/cards/card_XX.png",
      playerOne1: "./img/cards/card_XX.png",
      playerOne2: "./img/cards/card_XX.png",
      playerTwo1: "./img/cards/card_XX.png",
      playerTwo2: "./img/cards/card_XX.png",
      equity: hands(equity[0]).equity
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleEnter = this.handleEnter.bind(this);
    this.reset = this.reset.bind(this);
    this.next = this.next.bind(this)
  }

  componentWillMount = () => {
    localStorage.setItem('startTime', 0);
    localStorage.setItem('stopTime', 0);
    if(!localStorage.hands){
      localStorage.setItem('hands', 0)
    }
    if(!localStorage.totalTime){
      localStorage.setItem('totalTime', 0)
    }
    if(!localStorage.totalDiff){
      localStorage.setItem('totalDiff', 0)
    }
  }

  handleChange(e) {
    this.setState({
      value: e.target.value
    });
  }

  handleFocus() {
    this.setState({
      value: "",
      showScoreBoard: false
    });
  }

  handleBlur() {
    this.setState({
      showScoreBoard: true
    })
  }

  setRandomBoard() {
    const num = Math.floor(Math.random() * (equity.length - 1) + 1);
    this.setState(
      {
        value: "",
        board1: `./img/cards/card_${hands(equity[num]).board1}.png`,
        board2: `./img/cards/card_${hands(equity[num]).board2}.png`,
        board3: `./img/cards/card_${hands(equity[num]).board3}.png`,
        board4: `./img/cards/card_${hands(equity[num]).board4}.png`,
        playerOne1: `./img/cards/card_${hands(equity[num]).playerOne1}.png`,
        playerOne2: `./img/cards/card_${hands(equity[num]).playerOne2}.png`,
        playerTwo1: `./img/cards/card_${hands(equity[num]).playerTwo1}.png`,
        playerTwo2: `./img/cards/card_${hands(equity[num]).playerTwo2}.png`,
        equity: hands(equity[num]).equity
      },
      () => console.log(equity[num], this.state.equity)
    );
  }

  handleEnter(e) {
    e.preventDefault()
    if(!this.state.sessionStarted){
      this.setState({
        sessionStarted: true,
      })
    }
    if(!this.state.handStarted){
      localStorage.startTime = Date.now();
      this.setRandomBoard()
      document.querySelector('input').focus()
      this.setState({
        handStarted: true,

      })
    }
    if(this.state.handStarted){
      localStorage.setItem('stopTime', Date.now())
      this.setState({
        timeToAnswer: (Number(localStorage.stopTime) - Number(localStorage.startTime)),
        hands: Number(this.state.hands) + 1,
        handStarted: false,
        showMessage: true
      })
      localStorage.setItem('hands', Number(localStorage.getItem('hands')) + 1)
      localStorage.setItem('totalTime', (Number(localStorage.getItem('totalTime') / 1000) + (Number(localStorage.stopTime) - Number(localStorage.startTime))))
      localStorage.setItem('totalDiff', (Number(localStorage.getItem('TotalDiff')) + Math.abs((Number(this.state.value) - Number(this.state.equity)))))
      this.setState(() => ({
        totalDiff: localStorage.getItem('totalDiff'),
        totalTime: localStorage.getItem('totalTime')
      }))
    }
  }

  next() {
    if(!this.state.handStarted){
      localStorage.startTime = Date.now();
      this.setRandomBoard()
      document.querySelector('input').focus()
      this.setState({
        handStarted: true,
        showMessage: false
      })
    }
  }

  reset() {
    localStorage.setItem('hands', 0)
    localStorage.setItem('totalTime', 0)
    localStorage.setItem('totalDiff', 0)
    this.setState({
      value: '',
      sessionStarted: false,
      showScoreBoard: true,
      showMessage: false,
      hands: 0,
      totalDiff: 0,
      totalTime: 0,
      equity: 0,
      board1: "./img/cards/card_XX.png",
      board2: "./img/cards/card_XX.png",
      board3: "./img/cards/card_XX.png",
      board4: "./img/cards/card_XX.png",
      playerOne1: "./img/cards/card_XX.png",
      playerOne2: "./img/cards/card_XX.png",
      playerTwo1: "./img/cards/card_XX.png",
      playerTwo2: "./img/cards/card_XX.png"
    });
  }

  render() {
    return (
      <div className="App">
        <Header headerText={this.state.headerText} />
        <Board
          card1={this.state.board1}
          card2={this.state.board2}
          card3={this.state.board3}
          card4={this.state.board4}
        />
        <form onSubmit={this.handleEnter}>
        <div className="field">
          <Hand card1={this.state.playerOne1} card2={this.state.playerOne2} />
          <InputField
            value={this.state.value}
            handleChange={this.handleChange}
            handleFocus={this.handleFocus}
            handleBlur={this.handleBlur}
          />
          <Hand card1={this.state.playerTwo1} card2={this.state.playerTwo2} />
        </div>
        <div className="buttonField">
          <button className="btn" onClick={this.reset} type="button">
            Reset
          </button>
          <button className="btn"  type="submit">
            {this.state.sessionStarted ? "Enter" : "Start"}
          </button>
          <button className="btn" onClick={this.next} type="button" disabled={this.state.handStarted}>
            Next
          </button>
        </div>
        </form>
        {this.state.showMessage && <Message guess={Number(this.state.value).toFixed(2)} time={(this.state.timeToAnswer/1000).toFixed(2)} />}
        {this.state.showScoreBoard && 
          <ScoreBoard 
            hands={this.state.hands} 
            eq={!this.state.handStarted && (this.state.equity).toFixed(2)} 
            diff={!this.state.handStarted && (Math.abs(this.state.value - this.state.equity)).toFixed(2)}
            avgDiff={!this.state.handStarted && this.state.hands ? Number((this.state.totalDiff) / this.state.hands).toFixed(2) : '0.00'}
            avgTime={!this.state.handStarted && this.state.hands ? Number(((this.state.totalTime) / 1000) / this.state.hands).toFixed(2) : '0.00'}
          />}
      </div>
    );
  }
}

export default App;
