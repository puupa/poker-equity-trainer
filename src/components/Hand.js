import React from 'react'
import Card from './Card'
import '../css/Hand.css'

const Hand = (props) => (
    <div className="Hand-container">
        <Card card={props.card1} />
        <Card card={props.card2} />
    </div>
)

export default Hand