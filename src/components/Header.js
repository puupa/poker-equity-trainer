import React from 'react'
import '../css/Header.css'

const Header = (props) => (
    <div className="Header-container">
        <h1>Poker Equity Trainer</h1>
    </div>
)

export default Header