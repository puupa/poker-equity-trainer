import React from "react";
import "../css/ScoreBoard.css";

const ScoreBoard = props => {
  return (
      <table className="table table-dark">
        <thead>
          <tr>
            <th scope="col">Hands</th>
            <th scope="col">Eq</th>
            <th scope="col">Diff</th>
            <th scope="col">Avg. Diff</th>
            <th scope="col">Avg. Time</th>
          </tr>
        </thead>
        <tbody>
          <tr className="info">
            <th scope="row">{props.hands}</th>
            <td>{props.eq}</td>
            <td>{props.diff}</td>
            <td>{props.avgDiff}</td>
            <td>{props.avgTime}</td>
          </tr>
        </tbody>
      </table>
  );
};

export default ScoreBoard;
