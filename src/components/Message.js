import React from 'react'
import '../css/Message.css'

const Message = (props) => {
    return (
        <p>You guessed {props.guess}% in {props.time} seconds</p>
    )
}

export default Message