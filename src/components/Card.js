import React from 'react'
import '../css/Card.css'

const Card = (props) => {
    return (
        <div className="Card-container">
            <img src={props.card} alt="PlayingCard" />
        </div>
    )
}

export default Card